# This is the list of attacks conducted during the capture of the ZBDS 2023
# zigbee dataset.

import pandas as pd
# Attacks that were specifically studied have their start time with second resolution.
all_attacks = [
    # Attack Session 01
    {'sess': '01', 'type': 'A', 'start': pd.Timestamp('2022-07-03 07:23+02:00')},
    {'sess': '01', 'type': 'B', 'start': pd.Timestamp('2022-07-03 07:27+02:00')},
    {'sess': '01', 'type': 'C', 'start': pd.Timestamp('2022-07-03 09:12+02:00')},
    {'sess': '01', 'type': 'D', 'start': pd.Timestamp('2022-07-03 10:28:13+02:00')},
    {'sess': '01', 'type': 'E', 'start': pd.Timestamp('2022-07-03 10:36+02:00')},
    # Attack Session 02
    {'sess': '02', 'type': 'A', 'start': pd.Timestamp('2022-07-04 10:23+02:00')},
    {'sess': '02', 'type': 'B', 'start': pd.Timestamp('2022-07-04 10:26+02:00')},
    {'sess': '02', 'type': 'C', 'start': pd.Timestamp('2022-07-04 11:35+02:00')},
    {'sess': '02', 'type': 'D', 'start': pd.Timestamp('2022-07-04 12:08:10+02:00')},
    {'sess': '02', 'type': 'E', 'start': pd.Timestamp('2022-07-04 12:12+02:00')},
    # Attack Session 03
    {'sess': '03', 'type': 'A', 'start': pd.Timestamp('2022-07-05 06:55+02:00')},
    {'sess': '03', 'type': 'B', 'start': pd.Timestamp('2022-07-05 06:58+02:00')},
    {'sess': '03', 'type': 'C', 'start': pd.Timestamp('2022-07-05 07:53+02:00')},
    {'sess': '03', 'type': 'D', 'start': pd.Timestamp('2022-07-05 08:09:07+02:00')},
    {'sess': '03', 'type': 'E', 'start': pd.Timestamp('2022-07-05 08:11+02:00')},
    # Attack Session 04
    {'sess': '04', 'type': 'A', 'start': pd.Timestamp('2022-07-05 10:50+02:00')},
    {'sess': '04', 'type': 'B', 'start': pd.Timestamp('2022-07-05 10:52+02:00')},
    {'sess': '04', 'type': 'C', 'start': pd.Timestamp('2022-07-05 11:14+02:00')},
    {'sess': '04', 'type': 'D', 'start': pd.Timestamp('2022-07-05 12:05:06+02:00')},
    {'sess': '04', 'type': 'E', 'start': pd.Timestamp('2022-07-05 12:07+02:00')},
    # Attack Session 05
    {'sess': '05', 'type': 'A', 'start': pd.Timestamp('2022-07-06 07:53+02:00')},
    {'sess': '05', 'type': 'B', 'start': pd.Timestamp('2022-07-06 07:55+02:00')},
    {'sess': '05', 'type': 'C', 'start': pd.Timestamp('2022-07-06 08:25+02:00')},
    {'sess': '05', 'type': 'D', 'start': pd.Timestamp('2022-07-06 09:01:14+02:00')},
    {'sess': '05', 'type': 'E', 'start': pd.Timestamp('2022-07-06 09:03+02:00')},
    # Attack Session 06
    {'sess': '06', 'type': 'A', 'start': pd.Timestamp('2022-07-06 16:24+02:00')},
    {'sess': '06', 'type': 'B', 'start': pd.Timestamp('2022-07-06 16:25+02:00')},
      # Attack 06C was not conducted
    {'sess': '06', 'type': 'D', 'start': pd.Timestamp('2022-07-06 18:04:01+02:00')},
    {'sess': '06', 'type': 'E', 'start': pd.Timestamp('2022-07-06 18:06+02:00')},
    # Attack Session 07
    {'sess': '07', 'type': 'A', 'start': pd.Timestamp('2022-07-07 07:36+02:00')},
    {'sess': '07', 'type': 'B', 'start': pd.Timestamp('2022-07-07 07:38+02:00')},
    {'sess': '07', 'type': 'C', 'start': pd.Timestamp('2022-07-07 08:02+02:00')},
    {'sess': '07', 'type': 'D', 'start': pd.Timestamp('2022-07-07 09:06:01+02:00')},
    {'sess': '07', 'type': 'E', 'start': pd.Timestamp('2022-07-07 09:08:01+02:00')},
    # Attack Session 08
    {'sess': '08', 'type': 'A', 'start': pd.Timestamp('2022-07-08 07:49+02:00')},
    {'sess': '08', 'type': 'B', 'start': pd.Timestamp('2022-07-08 07:52+02:00')},
    {'sess': '08', 'type': 'C', 'start': pd.Timestamp('2022-07-08 08:23:09+02:00')},
    {'sess': '08', 'type': 'D', 'start': pd.Timestamp('2022-07-08 09:52:03+02:00')},
    {'sess': '08', 'type': 'E', 'start': pd.Timestamp('2022-07-08 09:54:03+02:00')},
    # Attack Session 09
    {'sess': '09', 'type': 'A', 'start': pd.Timestamp('2022-07-08 15:14+02:00')},
    {'sess': '09', 'type': 'B', 'start': pd.Timestamp('2022-07-08 15:15+02:00')},
    {'sess': '09', 'type': 'C', 'start': pd.Timestamp('2022-07-08 19:35:05+02:00')},
    {'sess': '09', 'type': 'D', 'start': pd.Timestamp('2022-07-08 20:23:10+02:00')},
    {'sess': '09', 'type': 'E', 'start': pd.Timestamp('2022-07-08 20:25:10+02:00')},
    # Attack Session 10
    {'sess': '10', 'type': 'A', 'start': pd.Timestamp('2022-07-08 21:17+02:00')},
    {'sess': '10', 'type': 'B', 'start': pd.Timestamp('2022-07-08 21:19+02:00')},
    {'sess': '10', 'type': 'C', 'start': pd.Timestamp('2022-07-08 22:28:10+02:00')},
    {'sess': '10', 'type': 'D', 'start': pd.Timestamp('2022-07-08 23:15:07+02:00')},
    {'sess': '10', 'type': 'E', 'start': pd.Timestamp('2022-07-08 23:17:05+02:00')}
]