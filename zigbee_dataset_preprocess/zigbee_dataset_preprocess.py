# This python 3 example script preprocesses a PCAP file containing Zigbee frames
# in order to produce a JSON file containing only some selected frames and for
# each of them only a few fields of interest. It uses pyshark package.
#
# Frames must have been recorded with a sniffer like TI CC2531 and its firmware 
# must replace the first FCS byte by RSSI and the first bit of second FCS byte
# by a FCS_OK information. See page 230 of swru191f.pdf.
# In adequation, if it is used, Wireshark IEEE 802.15.4 preferences concerning
# FCS Format must be set to "TI CC24xx metadata."
#
# To be retained, a frame must meet the following requirements:
# * It must have a wpan.fcs_ok field.
# * Its wpan.fcs_ok field must be valid.
# * It must have a wpan.src16 field. For instance, ACK frames will be excluded.
# * Its wpan.security field must be 0.
# * Its wpan.frame_type must be between 0 and 3.
# In wireshark, the retained frames can be visualized with the following filter:
# wpan.fcs_ok and !wpan.fcs.bad and 
#                       wpan.src16 and wpan.security==0 and wpan.frame_type<=3
#
# When a frame is retained, only the following fields are saved:
# * frame_index: this is the Wireshark "No" column value minus 1.
# * src16: this is the 16-bit MAC-layer identifier of the frame emitter.
# * length: length of the frame, in bytes.
# * time_relative_us: the time in µs elapsed since first frame.
# * rssi: the rssi of the frame with offset susbstraction done (see page 233 of
#   swru191f.pdf).
# * epoch_us: the epoch time in microseconds the frame is received by sniffer.
# 
# A frame in the JSON output file is a list with the 6 aforementioned fields.
# The complete file is a list of frames, hence a list of lists.
#
# Usage example
# $ python zigbee_dataset_preprocess.py rpi2-2022-07-01-18-00-00.pcap
# A few things will be printed to stdout and
# rpi2-2022-07-01-18-00-00.json will be created, ready for testing algorithms!
#
# Note
# From here, "packet" is improperly used instead of "frame".

import sys, os
import pyshark
import json

def main():
    """main function"""
    if len(sys.argv) != 2:
        print ("Usage: {} <file.pcap>".format(sys.argv[0]))
        exit(1)
    c = pyshark.FileCapture(input_file=sys.argv[1])
    print()
    print('-------------------------------------------------------------------------------------------')
    print('A RSSI extractor on IEEE 802.15.4 pcap files featuring "TI CC24xx metadata" as "FCS Format"')
    print('-------------------------------------------------------------------------------------------')
    print()
    
    wpan_no_fcs_count = 0
    wpan_fcs_ok_count = 0
    wpan_bad_fcs_count = 0
    wpan_src16_count = 0
    wpan_no_src16_count = 0
    wpan_security_count = 0
    wpan_bad_frame_type_count = 0
    device_addresses = set()
    retained_packets = []
    
    for pkt_index, pkt in enumerate(c):
        if hasattr(pkt.wpan, 'fcs_ok'):
            if(pkt.wpan.fcs_ok == '1'):
                wpan_fcs_ok_count+=1
                if hasattr(pkt.wpan, 'src16'):
                    wpan_src16_count+=1
                    address = pkt.wpan.src16
                    if(pkt.wpan.security=='0'):
                        frame_type = int(pkt.wpan.frame_type, base=16)
                        if (frame_type >= 0 and frame_type <= 3):
                            device_addresses.add(address)
                            retained_packets.append([pkt_index, int(pkt.wpan.src16, 0), int(pkt.frame_info.len), int(1e6*float(pkt.frame_info.time_relative)), int(pkt.wpan.rssi)-73, int(1e6*float(pkt.frame_info.time_epoch))])
                        else:
                            print('Packet {:7d}: has type btw 4 and 7.'.format(pkt_index))
                            wpan_bad_frame_type_count+=1
                    else:
                        print('Packet {:7d}: has wpan.security = 1.'.format(pkt_index))
                        wpan_security_count+=1
                else:
                    wpan_no_src16_count+=1
            else:
                wpan_bad_fcs_count+=1
                print('Packet {:7d}: bad FCS occured.'.format(pkt_index))
        else:
            print('Packet {:7d}: has no wpan.fcs_ok field, probably a malformed packet.'.format(pkt_index))
            wpan_no_fcs_count+=1
    wpan_packet_count = pkt_index + 1
    print()
    print('Capture {}: {} packets'.format(c, wpan_packet_count))
    print()
    print('Processed {} packets:'.format(wpan_packet_count))
    print('{:7d}\t{:8.2%}\tNo FCS (malformed packet)'.format(wpan_no_fcs_count,wpan_no_fcs_count / wpan_packet_count))
    print('{:7d}\t{:8.2%}\tBAD FCS'.format(wpan_bad_fcs_count, wpan_bad_fcs_count / wpan_packet_count))
    print('{:7d}\t{:8.2%}\tSRC16 address'.format(wpan_src16_count, wpan_src16_count / wpan_packet_count))
    print('\t(among them: {:7d} Security Enabled packets, not retained)'.format(wpan_security_count))
    print('\t(among them: {:7d} Bad Frame Type packets, not retained)'.format(wpan_bad_frame_type_count))
    print('{:7d}\t{:8.2%}\tNo SRC16 address'.format(wpan_no_src16_count, wpan_no_src16_count / wpan_packet_count))
    wpan_packet_count_check = wpan_no_fcs_count + wpan_bad_fcs_count + wpan_src16_count + wpan_no_src16_count
    print('{:7d}\t{:8.2%}\tTOTAL'.format(wpan_packet_count_check, wpan_packet_count_check / wpan_packet_count))
    print()
    print('Found following SRC16 addresses:')
    print(device_addresses)
    print()
    print('Working on {} light packets.'.format(len(retained_packets)))
    print(retained_packets[:10])
    print('...')
    print()
    temp = os.path.splitext(sys.argv[1])[0]
    base = os.path.basename(temp) 
    filename = base + '.json'
    with open(filename, 'w') as f:
        json.dump(retained_packets, f)
    print("Created file {}".format(filename))

    c.close()
    
if __name__ == '__main__':
    main()
