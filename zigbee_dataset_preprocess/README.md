# Description

This python 3 example script preprocesses a PCAP file containing Zigbee frames in order to produce a JSON file containing only some selected frames and for each of them only a few fields of interest. They are detailed in the heading comments inside the python script.

Of course, PCAP files can be [merged](https://www.wireshark.org/docs/wsug_html_chunked/ChIOMergeSection.html) before proceeding to preprocess.

# Dependencies
* [pyshark](https://github.com/KimiNewt/pyshark/)

# Usage
`python zigbee_dataset_preprocess.py rpi2-2022-07-01-18-00-00.pcap`

It should produce the file `rpi2-2022-07-01-18-00-00.json` (reproduced here), ready to be used with [some notebooks](../zigbee_dataset_notebooks/).