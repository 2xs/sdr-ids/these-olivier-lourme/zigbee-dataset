# Description

This repository contains resources to start using the ZBDS2023 Zigbee dataset we made available at [doi.org/10.57745/NDW74U](https://doi.org/10.57745/NDW74U). It is organised this way:

- [attacks_references.py](attacks_references.py) references the attacks we injected during the dataset capture,
- [zigbee_dataset_preprocess/](zigbee_dataset_preprocess/) is an exemple of how to process PCAP files from the dataset in order to produce JSON files with only certain fields of certain selected frames,
- [zigbee_dataset_notebooks/](zigbee_dataset_notebooks/) contains to date one Jupyter notebook using ZBDS2023, as a tutorial.

A paper introducing the dataset has been presented on June 21, 2023 at [IEEE WiMob 2023 conference](http://www.wimob.org/wimob2023/programme.html). It is available from [HAL](https://hal.science/hal-04173958).